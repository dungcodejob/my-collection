import { Routes } from "@angular/router";
import { authGuard } from "@core/guards";

export const routes: Routes = [
  {
    path: "",
    redirectTo: "home",
    pathMatch: "full",
  },
  {
    path: "home",
    loadComponent: () => import("@features/layout/layout.component"),
    canActivate: [authGuard],
    loadChildren: () =>
      import("@features/bookmark/bookmark.routes").then(m => m.bookmarkRoutes),
  },
  {
    path: "security",
    loadChildren: () =>
      import("@features/security/security.routes").then(m => m.securityRoutes),
  },
];
