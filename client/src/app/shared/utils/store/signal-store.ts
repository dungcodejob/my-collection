import {
  DestroyRef,
  Injector,
  Signal,
  WritableSignal,
  computed,
  inject,
  signal,
  untracked,
} from "@angular/core";
import { Observable, Subscription } from "rxjs";
import { createEffect } from "./create-effect";

type SignalState<T> = { [P in keyof T]: WritableSignal<T[P]> };
export class SignalStore<TState extends Record<string, unknown>> {
  protected readonly _injector = inject(Injector);
  protected readonly _destroyRef = inject(DestroyRef);
  protected readonly _$state: SignalState<TState>;

  constructor(initialState: TState) {
    const $state: Partial<SignalState<TState>> = {};
    const keys = Object.keys(initialState) as (keyof TState)[];

    keys.forEach(key => {
      $state[key] = signal<TState[keyof TState]>(initialState[key]);
    });

    this._$state = $state as SignalState<TState>;
  }

  /**
   * Returns the state as a signal
   */
  get $state(): Signal<TState> {
    return computed(() => {
      const keys = Object.keys(this._$state) as (keyof TState)[];

      return keys.reduce((results: Partial<TState>, key: keyof TState) => {
        results[key] = this._$state[key]();
        return results;
      }, {}) as TState;
    });
  }

  /**
   * Returns the state as a snapshot
   * This will read through all the signals
   */
  get snapshots(): TState {
    return untracked(() => this.$state());
  }

  // protected initialize<K extends keyof TState>(state: TState): void {
  //   const signals: Partial<SignalState<TState>> = {};
  //   const keys = Object.keys(signals) as K[];

  //   keys.forEach(key => {
  //     signals[key] = signal<TState[K]>(state[key]);
  //   });

  //   this._signals = signals as SignalState<TState>;
  // }

  /**
   * Selects a single piece of the state as a Signal and optionally maps it to a new signal
   * @param fn: The function accepts signals state and returns the pieces of state (computed signal) we want to retrieve.
   * @param cb: (Optional) The callback function that will map to the computed signal
   */
  select<K extends keyof TState, P extends TState[K]>(
    fn: (state: SignalState<TState>) => Signal<P>
  ): Signal<P>;
  select<K extends keyof TState, P extends TState[K], S>(
    fn: (state: SignalState<TState>) => Signal<P>,
    cb: (s: P) => S
  ): Signal<S>;
  select<K extends keyof TState, P extends TState[K], S>(
    fn: (state: SignalState<TState>) => Signal<P>,
    cb?: (s: P) => S
  ): Signal<S | P> {
    return computed(() => {
      const $selected = fn(this._$state);
      return cb ? cb($selected()) : $selected();
    });
  }

  /**
   * Selects multiple pieces of the state as a computed Signal and optionally maps it to a new signal
   * @param fn: TThe function accepts signals state and returns object state as a signal
   * @param cb: (Optional) The callback function that will map to the computed signal
   */
  selectMany<P extends Partial<TState>>(
    fn: (state: SignalState<TState>) => SignalState<P>
  ): Signal<P>;
  selectMany<P extends Partial<TState>, S>(
    fn: (state: SignalState<TState>) => SignalState<P>,
    cb: (s: P) => S
  ): Signal<S>;
  selectMany<P extends Partial<TState>, S>(
    fn: (state: SignalState<TState>) => SignalState<P>,
    cb?: (s: P) => S
  ): Signal<P | S> {
    return computed(() => {
      const $selected = fn(this._$state);

      const keys = Object.keys($selected) as (keyof TState)[];
      const results = keys.reduce((obj: P, key) => {
        obj[key] = $selected[key]();
        return obj;
      }, {} as P);

      return cb ? cb(results) : results;
    });
  }

  /**
   * This method is ideal to pick pieces of state from somewhere else
   * It will return an object ct that contains properties as signals.
   * Used best in combination with the connect method
   * @param fn: The function accepts signals and returns the pieces of state (origin signal) we want to retrieve.
   */
  pick<K extends keyof TState, P extends TState[K]>(
    fn: (state: SignalState<TState>) => WritableSignal<P>
  ): Signal<P> {
    return fn(this._$state).asReadonly();
  }

  effect<
    ProvidedType = void,
    OriginType extends Observable<ProvidedType> | unknown = Observable<ProvidedType>,
    ObservableType = OriginType extends Observable<infer A> ? A : never,
    ReturnType = ProvidedType | ObservableType extends void
      ? (observableOrValue?: ObservableType | Observable<ObservableType>) => Subscription
      : (observableOrValue: ObservableType | Observable<ObservableType>) => Subscription
  >(generator: (origin$: OriginType) => Observable<unknown>): ReturnType {
    return createEffect(generator, this._injector);
  }

  protected updater(fn: (state: TState) => Partial<TState>): void {
    const stateToUpdate = fn(this.snapshots);
    const keys = Object.keys(stateToUpdate) as (keyof TState)[];

    keys.forEach(key => {
      this._$state[key].set(stateToUpdate[key] as TState[keyof TState]);
    });
  }

  /**
   * Patches the state with a partial object.
   * This will loop through all the state signals and update
   * them one by one
   */
  protected patch<P extends keyof TState>(object: Partial<TState>): void {
    const keys = Object.keys(object) as P[];

    keys.forEach(key => {
      this._$state[key].set(object[key] as TState[P]);
    });
  }

  // private throwOrReturnSignals(): SignalState<TState> {
  //   if (!this._$state) {
  //     throw new Error(
  //       "Signal state is not initialized yet, call the initialize() method before using any other methods"
  //     );
  //   }

  //   return this._$state;
  // }
}
