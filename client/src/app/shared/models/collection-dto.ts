import { plainToInstance } from "class-transformer";
import { DocumentData, WithFieldValue } from "firebase/firestore";
import { BaseConverter } from "./base-converter";
import { BaseDto } from "./base-dto";

export class CollectionDto extends BaseDto {
  public static Name = "collections";

  name!: string;
  icon!: string;
}

export type CollectionToAdd = Omit<CollectionDto, keyof BaseDto>;
export type CollectionToUpdate = Omit<CollectionDto, keyof BaseDto>;
export type CollectionToDelete = CollectionDto["id"];

export class CollectionConverter extends BaseConverter<CollectionDto> {
  override transformToFirestore<CollectionDto>(
    model: WithFieldValue<CollectionDto>
  ): DocumentData {
    return model as DocumentData;
  }

  override transformFromFirestore(data: DocumentData): CollectionDto {
    return plainToInstance(CollectionDto, data);
  }
}
