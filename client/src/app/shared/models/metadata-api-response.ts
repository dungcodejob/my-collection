export interface MetadataApiResponse {
  title: string;
  description: string;
  domain: string;
  images: string[];
  favicon: string;
}
