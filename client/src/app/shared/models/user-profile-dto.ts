import { plainToInstance } from "class-transformer";
import { DocumentData, WithFieldValue } from "firebase/firestore";
import { BaseConverter } from "./base-converter";
import { BaseDto } from "./base-dto";

export class UserProfileDto extends BaseDto {
  public static Name = "users";

  email!: string;
  firstName!: string;
  lastName!: string;

  get fullName(): string {
    return this.lastName + " " + this.firstName;
  }
}

export class UserProfileConverter extends BaseConverter<UserProfileDto> {
  override transformToFirestore<UserProfileDto>(
    model: WithFieldValue<UserProfileDto>
  ): DocumentData {
    return model as DocumentData;
  }

  override transformFromFirestore(data: DocumentData): UserProfileDto {
    return plainToInstance(UserProfileDto, data);
  }
}
