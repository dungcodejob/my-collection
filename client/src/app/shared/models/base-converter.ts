// Third parties
import {
  DocumentData,
  QueryDocumentSnapshot,
  SnapshotOptions,
  WithFieldValue,
} from "firebase/firestore";

// Project
import { BaseDto } from "./base-dto";

export interface FirestoreConverter<T> {
  toFirestore(model: WithFieldValue<T>): DocumentData;
  fromFirestore(snapshot: QueryDocumentSnapshot, options: SnapshotOptions): T;
}

export abstract class BaseConverter<T extends BaseDto> implements FirestoreConverter<T> {
  toFirestore(model: WithFieldValue<T>): DocumentData {
    const transformed = this.transformToFirestore(model);
    // remove id from model
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { id: _, ...filtered } = transformed;
    return filtered;
  }

  fromFirestore(snapshot: QueryDocumentSnapshot, options: SnapshotOptions): T {
    const data = snapshot.data(options);
    const transformed = this.transformFromFirestore(data);
    transformed.id = snapshot.id;
    return transformed;
  }

  abstract transformToFirestore<T extends BaseDto>(
    model: WithFieldValue<T>
  ): DocumentData;

  abstract transformFromFirestore(data: DocumentData): T;
}
