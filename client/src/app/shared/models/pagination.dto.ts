import { DocumentSnapshot } from "firebase/firestore";

export type PaginationDto = {
  cursor: DocumentSnapshot | null;
  direction: "prev" | "next" | null;
  size: number;
};
