// Third parties
import { Transform } from "class-transformer";

export class BaseDto {
  id!: string;
  @Transform(({ value }) => value.toDate())
  updatedAt: Date = new Date();

  @Transform(({ value }) => value.toDate())
  createdAt: Date = new Date();
}

// Angular
// import { inject } from "@angular/core";

// // Third parties
// import {
//   CollectionReference,
//   DocumentData,
//   Firestore,
//   QueryDocumentSnapshot,
//   SnapshotOptions,
//   Timestamp,
//   collection,
// } from "firebase/firestore";
// import { Transform } from "class-transformer";

// // Project
// import { BaseSchema } from "./base-schema";

// export const TransformTimestamp = () =>
//   Transform(params => (params.value as Timestamp).toDate());

// export class BaseDto implements BaseSchema {
//   id!: string;
//   @TransformTimestamp()
//   updatedAt: Date;
//   @TransformTimestamp()
//   createdAt: Date;

//   constructor() {
//     this.updatedAt = new Date();
//     this.createdAt = new Date();
//   }

//   static Name: string;
//   static get collection(): CollectionReference {
//     const firestore = inject(Firestore);
//     return collection(firestore, this.Name).withConverter(this.converter);
//   }
//   private static get converter() {
//     return {
//       toFirestore: <T extends BaseDto>(model: T): DocumentData => {
//         return this.toFirestore(model);
//       },
//       fromFirestore: <T extends BaseDto>(
//         snapshot: QueryDocumentSnapshot,
//         options: SnapshotOptions
//       ): T => {
//         return this.fromFirestore(snapshot, options);
//       },
//     };
//   }

//   private static toFirestore<T extends BaseDto>(model: T): Record<string, any> {
//     const transformed = this.transformToFirestore(model);

//     // remove id from model
//     // eslint-disable-next-line @typescript-eslint/no-unused-vars
//     const { id: _, ...filtered } = transformed;
//     return filtered;
//   }

//   private static fromFirestore<T extends BaseDto>(
//     snapshot: QueryDocumentSnapshot,
//     options: SnapshotOptions
//   ): T {
//     const data = snapshot.data(options);
//     const transformed = this.transformFromFirestore(data) as T;
//     transformed.id = snapshot.id;

//     return transformed;
//   }

//   protected static transformToFirestore<T extends BaseDto>(
//     model: T
//   ): Record<string, any> {
//     return model;
//   }

//   protected static transformFromFirestore(data: DocumentData): Record<string, any> {
//     return data;
//   }
// }
