import { plainToInstance } from "class-transformer";
import { DocumentData, WithFieldValue } from "firebase/firestore";
import { BaseConverter } from "../base-converter";
import { BookmarkDto } from "./bookmark-dto";

export class BookmarkConverter extends BaseConverter<BookmarkDto> {
  override transformToFirestore<BookmarkDto>(
    model: WithFieldValue<BookmarkDto>
  ): DocumentData {
    return model as DocumentData;
  }

  override transformFromFirestore(data: DocumentData): BookmarkDto {
    return plainToInstance(BookmarkDto, data);
  }
}
