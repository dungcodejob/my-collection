import { PaginationDto } from "../pagination.dto";

export type BookmarkQueryDto = PaginationDto;
