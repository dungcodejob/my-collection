import { BaseDto } from "../base-dto";

export class BookmarkDto extends BaseDto {
  public static Name = "bookmarks";

  url!: string;
  title!: string;
  domain!: string;
  img!: string;
  description: string | null;
  favicon!: string;

  constructor() {
    super();
    this.description = null;
  }
}

export type BookmarkToAdd = Omit<BookmarkDto, keyof BaseDto>;
export type BookmarkToUpdate = BookmarkToAdd & { id: BookmarkDto["id"] };
export type BookmarkToDelete = BookmarkDto["id"];
