import { ApplicationConfig, importProvidersFrom } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { provideRouter } from "@angular/router";
import { AuthApi, AuthService, AuthStore } from "@core/authentication";
import { provideConfigInitializer, provideWithConfig } from "@core/config";
import { provideFirebaseInitializer } from "@core/firebase";
import { UserApi } from "@core/http";
import { LogService } from "@core/logger";
import { PadIconModule } from "pad-ui-lib/icon";
import { routes } from "./app.routes";

export const initAppConfig = (): ApplicationConfig => {
  return {
    providers: [
      importProvidersFrom([PadIconModule.forRoot(), BrowserAnimationsModule]),
      AuthService,
      AuthStore,
      AuthApi,
      UserApi,
      provideWithConfig(LogService),
      provideFirebaseInitializer(),
      provideConfigInitializer(),
      // provideRouter(routes, withPreloading(PreloadAllModules)),
      provideRouter(routes),
    ],
  };
};
