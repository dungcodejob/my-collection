import { FactoryProvider } from "@angular/core";
import { APP_WITH_CONFIG_ASYNC, EnvConfig } from "@core/config";
import { initializeApp } from "firebase/app";
import { of } from "rxjs";

export function provideFirebaseInitializer(): FactoryProvider {
  return {
    provide: APP_WITH_CONFIG_ASYNC,
    multi: true,
    useFactory: () => {
      return (config: EnvConfig) => of(initializeApp(config.firebase));
    },
  };
}
