import { InjectionToken } from "@angular/core";
import { getAuth } from "firebase/auth";

export const FIREBASE_AUTH = new InjectionToken("firebase auth", {
  factory: () => getAuth(),
});
