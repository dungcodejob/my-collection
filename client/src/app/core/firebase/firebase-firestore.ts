import { InjectionToken } from "@angular/core";
import { getFirestore } from "firebase/firestore";

export const FIREBASE_FIRESTORE = new InjectionToken("firebase firestore", {
  factory: () => getFirestore(),
});
