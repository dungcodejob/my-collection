import { FirebaseOptions } from "firebase/app";

export interface EnvConfig {
  env: "dev" | "staging" | "prod";
  firebase: FirebaseOptions;
}
