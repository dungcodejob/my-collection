import { filter, map } from "rxjs";
// Angular
import { Type, inject } from "@angular/core";

// Third parties
import {
  CollectionReference,
  DocumentReference,
  collection,
  doc,
} from "@firebase/firestore";

// Project
import { FIREBASE_FIRESTORE } from "@core/firebase";
import { LogService } from "@core/logger";
import { BaseDto, FirestoreConverter } from "@shared/models";
import {
  DocumentData,
  Query,
  QueryDocumentSnapshot,
  QuerySnapshot,
  SnapshotOptions,
  getDocs,
} from "firebase/firestore";
import { Observable, of } from "rxjs";

export class BaseApi<T extends BaseDto> {
  protected readonly _logService = inject(LogService);
  protected readonly _fbFirestore = inject(FIREBASE_FIRESTORE);
  protected readonly _converter: FirestoreConverter<T>;

  constructor(type: Type<FirestoreConverter<T>>) {
    this._converter = new type();
  }

  protected getCollectionRef(path: string): CollectionReference<T> {
    return collection(this._fbFirestore, path).withConverter(this._converter);
  }

  protected getDocRef(path: string): DocumentReference<T> {
    return doc(this._fbFirestore, path).withConverter(this._converter);
  }

  protected collectionData<T = DocumentData>(
    query: Query<T>,
    options?: SnapshotOptions
  ): Observable<T[]> {
    return this.collectionSnapshot(query).pipe(
      map(docs => this.mapSnapshotToData(docs, options))
    );
  }

  protected collectionSnapshot<T = DocumentData>(
    query: Query<T>
  ): Observable<QueryDocumentSnapshot<T, DocumentData>[]> {
    return new Observable<QuerySnapshot<T, DocumentData>>(observer => {
      getDocs(query)
        .then(results => observer.next(results))
        .catch(err => observer.error(err))
        .finally(() => observer.complete());
    }).pipe(
      map(snapshot => snapshot.docs),
      filter(
        (docs, index) =>
          index > 0 || docs.length === 0 || !docs.every(item => item.metadata.fromCache)
      )
    );
  }

  protected mapSnapshotToData<T = DocumentData>(
    docs: QueryDocumentSnapshot<T, DocumentData>[],
    options?: SnapshotOptions
  ): T[] {
    const data: T[] = [];
    docs.forEach(doc => {
      doc.metadata.fromCache;
      data.push(doc.data(options));
    });

    return data;
  }

  // protected handleResponse<T>(operation = "operation", result?: T) {
  //   return (source$: Observable<T>) =>
  //     source$.pipe(
  //       map((result: T) => ({ status: "success", data: result, error: null })),
  //       catchError((err: Error) => {
  //         this._logService.error(operation, err);

  //         const message = err.message;
  //         if (err instanceof FirebaseError) {
  //         }

  //         if (err instanceof HttpErrorResponse) {
  //         }
  //         return of({ status: "error", data: result as T, error: err.message });
  //       })
  //     );
  // }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  protected handleError<T>(operation = "operation", result?: T) {
    return (
      error: any
    ): Observable<{ status: "success" | "error"; data: T; error: string | null }> => {
      this._logService.error(operation, error);

      // TODO: better job of transforming error for user consumption
      // this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of({
        status: "success",
        data: result as T,
        error: null,
      });
    };
  }
}
