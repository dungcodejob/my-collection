// Angular
import { Injectable } from "@angular/core";

// Third parties
import { docData } from "rxfire/firestore";

// Project
import { UserProfileConverter, UserProfileDto } from "@shared/models";
import { BaseApi } from "./base.api";

@Injectable()
export class UserApi extends BaseApi<UserProfileDto> {
  constructor() {
    super(UserProfileConverter);
  }

  getProfile(id: string) {
    const ref = this.getDocRef(`${UserProfileDto.Name}/${id}`);
    return docData(ref);
  }
}
