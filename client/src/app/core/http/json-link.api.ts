import { HttpClient } from "@angular/common/http";
import { Injectable, inject } from "@angular/core";
import { MetadataApiResponse } from "@shared/models";

const GET_METADATA_URL = `https://jsonlink.io/api/extract`;

@Injectable()
export class JsonLinkApi {
  private readonly _http = inject(HttpClient);

  getMetadata(url: string) {
    return this._http.get<MetadataApiResponse>(GET_METADATA_URL, {
      params: { url },
    });
  }
}
