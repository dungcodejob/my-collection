import { Injectable } from "@angular/core";
import { CollectionConverter, CollectionDto, UserProfileDto } from "@shared/models";
import { collectionData } from "rxfire/firestore";
import { Observable } from "rxjs";
import { BaseApi } from "./base.api";

export interface CollectionPathParams {
  uid: string;
}

@Injectable()
export class CollectionApi extends BaseApi<CollectionDto> {
  constructor() {
    super(CollectionConverter);
  }

  findAll(params: CollectionPathParams): Observable<CollectionDto[]> {
    const path = this._buildPath(params);
    const ref = this.getCollectionRef(path);
    return collectionData(ref);
  }

  private _buildPath(params: CollectionPathParams) {
    return `${UserProfileDto.Name}/${params.uid}/${CollectionDto.Name}`;
  }
}
