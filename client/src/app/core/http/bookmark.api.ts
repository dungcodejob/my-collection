import { Injectable } from "@angular/core";
import {
  BookmarkConverter,
  BookmarkDto,
  BookmarkPathParams,
  BookmarkQueryDto,
  BookmarkToAdd,
  BookmarkToUpdate,
  CollectionDto,
  UserProfileDto,
} from "@shared/models";
import { plainToInstance } from "class-transformer";
import * as Firestore from "firebase/firestore";
import { Observable, defer, from, map } from "rxjs";
import { BaseApi } from "./base.api";

@Injectable()
export class BookmarkApi extends BaseApi<BookmarkDto> {
  constructor() {
    super(BookmarkConverter);
  }

  fetchAll(
    params: BookmarkPathParams,
    query: BookmarkQueryDto
  ): Observable<{
    items: BookmarkDto[];
    firstCursor: Firestore.DocumentSnapshot | null;
    lastCursor: Firestore.DocumentSnapshot | null;
  }> {
    const path = this._buildPath(params);

    const queries: Firestore.QueryConstraint[] = [
      Firestore.orderBy("createdAt", "desc"),
      Firestore.limit(query.size),
    ];
    if (query.cursor && query.direction === "next") {
      queries.push(Firestore.startAfter(query.cursor));
    }

    if (query.cursor && query.direction === "prev") {
      queries.push(Firestore.endBefore(query.cursor));
    }

    const fsQuery = Firestore.query(this.getCollectionRef(path), ...queries);

    return this.collectionSnapshot(fsQuery).pipe(
      map(docs => {
        if (docs.length >= query.size) {
          return {
            items: this.mapSnapshotToData(docs),
            firstCursor: docs[0],
            lastCursor: docs[docs.length - 1],
          };
        }

        if (docs.length > 0) {
          return {
            items: this.mapSnapshotToData(docs),
            firstCursor: docs[0],
            lastCursor: null,
          };
        }

        return {
          items: [],
          firstCursor: null,
          lastCursor: null,
        };
      })
    );
  }

  // fetchAll(
  //   params: BookmarkPathParams,
  //   query: BookmarkQueryDto
  // ): Observable<BookmarkDto[]> {
  //   const path = this._buildPath(params);

  //   if (query.visibleId !== null) {
  //     const docRef = this.getDocRef(`${path}/${query.visibleId}`);
  //     const lastVisible$ = doc(docRef);

  //     return lastVisible$.pipe(
  //       switchMap(d => {
  //         const queries: Firestore.QueryConstraint[] = [
  //           Firestore.orderBy("createdAt", "desc"),
  //         ];
  //         if (query.visibleId !== null && d) {
  //           if (query.direction === "next") {
  //             queries.push(Firestore.startAfter(d));
  //             queries.push(Firestore.limit(query.size));
  //           }

  //           if (query.direction === "prev") {
  //             queries.push(Firestore.endAt(d));
  //             queries.push(Firestore.limitToLast(query.size));
  //           }
  //         } else {
  //           queries.push(Firestore.limit(query.size));
  //         }
  //         const fbQuery = Firestore.query(this.getCollectionRef(path), ...queries);
  //         return this.collectionData(fbQuery).pipe(tap(result => console.log(result)));
  //       }),
  //       take(1)
  //     );
  //   }

  //   const fbQuery = Firestore.query(
  //     this.getCollectionRef(path),
  //     Firestore.orderBy("createdAt", "desc"),
  //     Firestore.limit(query.size)
  //   );
  //   return this.collectionData(fbQuery).pipe(take(1));
  // }

  create(params: BookmarkPathParams, body: BookmarkToAdd): Observable<BookmarkDto> {
    const path = this._buildPath(params);
    const collectionRef = this.getCollectionRef(path);

    const model = plainToInstance(BookmarkDto, body);

    return from(defer(() => Firestore.addDoc(collectionRef, model))).pipe(
      map(ref => {
        model.id = ref.id;
        return model;
      })
    );
  }

  update(params: BookmarkPathParams, body: BookmarkToUpdate) {
    const path = this._buildPath(params);
    const docRef = this.getDocRef(`${path}/${body}`);

    const model = plainToInstance(BookmarkDto, body);

    return from(defer(() => Firestore.updateDoc(docRef, { ...model }))).pipe(
      map(() => model)
    );
  }

  private _buildPath(params: { uid: string; collectionId: string }): string {
    let path = "";
    path = path + `${UserProfileDto.Name}/${params.uid}/`;
    path = path + `${CollectionDto.Name}/${params.collectionId}/`;
    path = path + `${BookmarkDto.Name}`;

    return path;
  }
}
