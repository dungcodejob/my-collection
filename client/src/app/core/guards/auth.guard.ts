import { inject } from "@angular/core";
import { CanMatchFn, Router } from "@angular/router";
import { AuthStore } from "@core/authentication";
import { map } from "rxjs";

export const authGuard: CanMatchFn = () => {
  const authStore = inject(AuthStore);
  const router = inject(Router);

  return authStore.authenticated$.pipe(
    map(isAuthenticated =>
      !isAuthenticated ? router.createUrlTree(["security/login"]) : true
    )
  );
};
