import { Injectable, inject } from "@angular/core";
import { FIREBASE_AUTH } from "@core/firebase";
import { User, signInWithEmailAndPassword, signOut } from "firebase/auth";
import { Observable, defer, from, map } from "rxjs";

export type FirebaseUser = User;

export type LoginBodyRequest = {
  email: string;
  password: string;
};

@Injectable({ providedIn: "root" })
export class AuthApi {
  private readonly _fbAuth = inject(FIREBASE_AUTH);

  login(body: LoginBodyRequest): Observable<FirebaseUser> {
    return from(
      defer(() => signInWithEmailAndPassword(this._fbAuth, body.email, body.password))
    ).pipe(map(credential => credential.user));
  }

  logout(): Observable<void> {
    return from(defer(() => signOut(this._fbAuth)));
  }
}
