import { Injectable, computed, signal } from "@angular/core";
import { UserProfileDto } from "@shared/models";
import { BehaviorSubject, map, shareReplay, skip } from "rxjs";

@Injectable({ providedIn: "root" })
export class AuthStore {
  private readonly _userSubject = new BehaviorSubject<UserProfileDto | null>(null);
  private readonly _$user = signal<UserProfileDto | null>(null);

  readonly authenticated$ = this._userSubject.asObservable().pipe(
    skip(1),
    map(user => (user ? true : false)),
    shareReplay(0)
  );

  readonly $user = computed(() => {
    const user = this._$user();
    if (!user) {
      throw new Error("must be authentication when in layout");
    }
    return user;
  });

  setUser(value: UserProfileDto | null) {
    this._$user.set(value);
    this._userSubject.next(value);
  }
}
