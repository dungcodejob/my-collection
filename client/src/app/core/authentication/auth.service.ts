import { Injectable, forwardRef, inject } from "@angular/core";
import { FIREBASE_AUTH } from "@core/firebase";
import { UserApi } from "@core/http";
import { RedirectService } from "@core/services/redirect.service";
import { onAuthStateChanged } from "firebase/auth";
import { Observable, of, switchMap, tap } from "rxjs";
import { FirebaseUser } from "./auth.api";
import { AuthStore } from "./auth.store";

@Injectable({ providedIn: "root" })
export class AuthService {
  private readonly _fbAuth = inject(forwardRef(() => FIREBASE_AUTH));
  private readonly _userApi = inject(UserApi);
  private readonly _authStore = inject(AuthStore);
  private readonly _redirectService = inject(RedirectService);

  initializer() {
    return new Observable<FirebaseUser | null>(observer => {
      onAuthStateChanged(
        this._fbAuth,
        user => observer.next(user),
        error => observer.error(error),
        () => observer.complete()
      );
    }).pipe(
      switchMap(user => (user ? this._userApi.getProfile(user.uid) : of(null))),
      tap(value => {
        if (value) {
          this._authStore.setUser(value);
          this._redirectService.goToHome();
        } else {
          this._authStore.setUser(null);
          this._redirectService.goToLogin();
        }
      })
    );
  }
}
