import { Routes } from "@angular/router";
import { noAuthGuard } from "@core/guards";

export const securityRoutes: Routes = [
  {
    path: "login",
    loadComponent: () =>
      import("./containers/login/login.component").then(m => m.LoginComponent),
    canActivate: [noAuthGuard],
  },
];
