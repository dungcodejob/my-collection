import { Component, DestroyRef, OnInit, inject } from "@angular/core";
import { takeUntilDestroyed } from "@angular/core/rxjs-interop";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from "@angular/forms";
import { AuthApi, AuthStore, LoginBodyRequest } from "@core/authentication";
import { RedirectService } from "@core/services/redirect.service";

import { PadButtonModule } from "pad-ui-lib/button";
import { PadCheckboxModule } from "pad-ui-lib/checkbox";
import { PadFormModule } from "pad-ui-lib/form";
import { PadIconModule } from "pad-ui-lib/icon";
import { LoginFormComponent } from "../login-form/login-form.component";

@Component({
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
  standalone: true,
  imports: [
    ReactiveFormsModule,
    PadIconModule,
    PadButtonModule,
    PadFormModule,
    PadCheckboxModule,

    LoginFormComponent,
  ],
})
export class LoginComponent implements OnInit {
  private readonly _destroyRef = inject(DestroyRef);
  private readonly _redirectService = inject(RedirectService);
  private readonly _authStore = inject(AuthStore);
  #authApi = inject(AuthApi);
  #fb = inject(FormBuilder);
  form!: FormGroup<{
    email: FormControl<string>;
    password: FormControl<string>;
  }>;

  ngOnInit(): void {
    this.form = this.#fb.group({
      email: this.#fb.control("", {
        nonNullable: true,
        validators: [Validators.required, Validators.email],
      }),
      password: this.#fb.control("", {
        nonNullable: true,
        validators: [Validators.required],
      }),
    });
  }

  onSubmit() {
    const body = this.form.value as LoginBodyRequest;
    this.#authApi.login(body).pipe(takeUntilDestroyed(this._destroyRef)).subscribe();
  }
}
