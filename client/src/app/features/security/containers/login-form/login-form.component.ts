import { takeUntilDestroyed } from "@angular/core/rxjs-interop";
// Angular
import {
  ChangeDetectionStrategy,
  Component,
  DestroyRef,
  OnInit,
  inject,
  signal,
} from "@angular/core";
import { FormBuilder, ReactiveFormsModule, Validators } from "@angular/forms";

// Rx
import {
  Subject,
  catchError,
  filter,
  finalize,
  map,
  of,
  startWith,
  switchMap,
  take,
  tap,
} from "rxjs";

// Third parties
import { PadButtonModule } from "pad-ui-lib/button";
import { PadCheckboxModule } from "pad-ui-lib/checkbox";
import { PadFormModule } from "pad-ui-lib/form";

// Project
import { NgIf } from "@angular/common";
import { AuthApi, LoginBodyRequest } from "@core/authentication";
import { getFirebaseAuthErrorMessage, updateValueAndValidity } from "@shared/utils";
import { TypedFormGroup } from "@shared/utils/typed-from-group";
import { FirebaseError } from "firebase/app";

type LoginForm = TypedFormGroup<LoginBodyRequest>;

@Component({
  selector: "app-login-form",
  templateUrl: "./login-form.component.html",
  styleUrls: ["./login-form.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [NgIf, ReactiveFormsModule, PadFormModule, PadButtonModule, PadCheckboxModule],
})
export class LoginFormComponent implements OnInit {
  private readonly _destroyRef = inject(DestroyRef);
  private readonly _fb = inject(FormBuilder);
  private readonly _authApi = inject(AuthApi);

  form!: LoginForm;
  formSubmitSubject = new Subject<Event>();
  $loading = signal(false);
  $error = signal<string | null>(null);

  ngOnInit(): void {
    this._initForm();

    this.formSubmitSubject
      .pipe(
        tap(() => this.form.markAsDirty()),
        switchMap(event =>
          this.form.statusChanges.pipe(
            startWith(this.form.status),
            filter(status => status !== "PENDING"),
            map(status => ({ status, event })),
            take(1)
          )
        ),
        tap(({ status, event }) => {
          if (status === "VALID") {
            this._submitForm(event);
          }

          if (status === "INVALID") {
            updateValueAndValidity(this.form);
          }
        }),
        takeUntilDestroyed(this._destroyRef)
      )
      .subscribe();
  }

  private _initForm() {
    this.form = this._fb.group({
      email: this._fb.control("", {
        nonNullable: true,
        validators: [Validators.required, Validators.email],
      }),
      password: this._fb.control("", {
        nonNullable: true,
        validators: [Validators.required],
      }),
    });
  }

  private _submitForm(event: Event) {
    // target.setAttribute("diabled") = true;

    this.form.disable();
    this.$loading.set(true);
    this.$error.set(null);

    const body = this.form.getRawValue();

    this._authApi
      .login(body)
      .pipe(
        catchError(err => {
          if (err instanceof FirebaseError) {
            this.$error.set(getFirebaseAuthErrorMessage(err));
          } else {
            this.$error.set(err.message);
          }
          return of();
        }),
        finalize(() => {
          this.form.reset({ email: body.email, password: "" });
          this.form.controls.password.markAsUntouched();
          this.$loading.set(false);
          this.form.enable();
        })
      )
      .subscribe();
  }
}
