import {
  ChangeDetectionStrategy,
  Component,
  DestroyRef,
  inject,
  signal,
} from "@angular/core";
import { takeUntilDestroyed } from "@angular/core/rxjs-interop";

import { NgFor } from "@angular/common";

import { AuthApi, AuthStore } from "@core/authentication";
import { CollectionApi } from "@core/http";
import { CollectionDto } from "@shared/models";
import { PadIconModule } from "pad-ui-lib/icon";
import { NavCollectionItemComponent } from "../nav-collection-item/nav-collection-item.component";
import { NavItemComponent, NavItemDetail } from "../nav-item/nav-item.component";

type NavItem<T = NavItemDetail> = { link: string; detail: T };

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [NgFor, NavItemComponent, NavCollectionItemComponent, PadIconModule],
  providers: [],
})
export class SidebarComponent {
  private readonly _$user = inject(AuthStore).$user;
  private readonly _$collectionApi = inject(CollectionApi);
  private readonly _destroyRef = inject(DestroyRef);
  private readonly _authApi = inject(AuthApi);

  navCollectionItems = signal<NavItem<CollectionDto>[]>([]);

  navItems = signal<NavItem[]>([
    {
      detail: {
        iconKey: "layers-three",
        text: "Inbox",
        key: Symbol(),
      },
      link: "",
    },
    {
      detail: {
        iconKey: "bell",
        text: "Activity",
        key: Symbol(),
      },
      link: "",
    },
    {
      detail: {
        iconKey: "settings",
        text: "Settings",
        key: Symbol(),
      },
      link: "",
    },
  ]);

  constructor() {
    this._$collectionApi.findAll({ uid: this._$user().id }).subscribe(collections => {
      const data = collections.map(item => ({
        link: `${item.id}/bookmark`,
        detail: item,
      }));
      this.navCollectionItems.set(data);
    });
  }

  onLogout(): void {
    this._authApi.logout().pipe(takeUntilDestroyed(this._destroyRef)).subscribe();
  }
}
