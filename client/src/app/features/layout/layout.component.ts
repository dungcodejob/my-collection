import { Component, inject } from "@angular/core";
import { RouterOutlet } from "@angular/router";
import { AuthStore } from "@core/authentication";
import { CollectionApi } from "@core/http";
import { SidebarComponent } from "./components/sidebar/sidebar.component";

@Component({
  selector: "app-layout",
  templateUrl: "./layout.component.html",
  styleUrls: ["./layout.component.scss"],
  standalone: true,
  imports: [RouterOutlet, SidebarComponent],
  providers: [CollectionApi],
})
class LayoutComponent {
  private readonly _authStore = inject(AuthStore);
}

export default LayoutComponent;
