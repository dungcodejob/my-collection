import { Injectable, computed, inject } from "@angular/core";
import { takeUntilDestroyed, toObservable } from "@angular/core/rxjs-interop";
import { AuthStore } from "@core/authentication";
import { BookmarkApi, JsonLinkApi } from "@core/http";
import {
  BookmarkDto,
  BookmarkToAdd,
  BookmarkToUpdate,
  MetadataApiResponse,
} from "@shared/models";
import { RouterStore } from "@shared/stores";
import { SignalStore, prefix } from "@shared/utils";
import { EMPTY, Observable, catchError, exhaustMap, tap, withLatestFrom } from "rxjs";
import { BookmarkSignalStore } from "./bookmark.store";

enum ViewStatus {
  Idle = "idle",
  Loading = "loading",
  Added = "added",
  Updated = "updated",
  RetrievedMetadata = "retrievedMetadata",
  Error = "error",
}

type BookmarkDetailState = {
  status: ViewStatus;
  data: BookmarkDto | null;
  metadata: MetadataApiResponse | null;
  error: string | null;
};

const initialState: BookmarkDetailState = {
  status: ViewStatus.Idle,
  data: null,
  metadata: null,
  error: null,
};

@Injectable()
export class BookmarkDetailSignalStore extends SignalStore<BookmarkDetailState> {
  private readonly _bookmarkApi = inject(BookmarkApi);
  private readonly _jsonLinkApi = inject(JsonLinkApi);
  private readonly _bookmarkStore = inject(BookmarkSignalStore);
  private readonly _authStore = inject(AuthStore);

  readonly $data = this.select(state => state.data);
  readonly $metadata = this.select(state => state.metadata);
  readonly $success = this.select(
    state => state.status,
    status => status === "added" || status === "updated"
  );
  readonly $loading = this.select(
    state => state.status,
    status => status === "loading"
  );
  readonly $error = this.select(state => state.error);

  constructor() {
    super(initialState);

    toObservable(
      computed(() => {
        const entities = this._bookmarkStore.$entities();
        const selectedId = this._bookmarkStore.$selectedId();

        return entities.find(entity => entity.id === selectedId) ?? null;
      }),
      { injector: this._injector }
    )
      .pipe(takeUntilDestroyed(this._destroyRef))
      .subscribe(value => {
        this.patch({ data: value });
      });
  }

  add = this.effect((add$: Observable<BookmarkToAdd>) =>
    add$.pipe(
      withLatestFrom(RouterStore.getCollectionId()),
      exhaustMap(([bookmarkToAdd, collectionId]) => {
        const uid = this._authStore.$user().id;
        return this._bookmarkApi.create({ collectionId, uid }, bookmarkToAdd).pipe(
          prefix(() => this.patch({ status: ViewStatus.Loading })),
          tap({
            next: results => {
              this.patch({ status: ViewStatus.Added, data: results });
            },
            error: err => this.patch({ status: ViewStatus.Error, error: err.message }),
          }),
          catchError(() => EMPTY)
        );
      })
    )
  );

  update = this.effect((update$: Observable<BookmarkToUpdate>) =>
    update$.pipe(
      withLatestFrom(RouterStore.getCollectionId()),
      exhaustMap(([bookmarkToUpdate, collectionId]) => {
        const uid = this._authStore.$user().id;
        return this._bookmarkApi.update({ collectionId, uid }, bookmarkToUpdate).pipe(
          prefix(() => this.patch({ status: ViewStatus.Loading })),
          tap({
            next: results => {
              this.patch({ status: ViewStatus.Updated, data: results });
            },
            error: err => this.patch({ status: ViewStatus.Error, error: err.message }),
          }),
          catchError(() => EMPTY)
        );
      })
    )
  );

  getMetadata = this.effect((url$: Observable<string>) =>
    url$.pipe(
      exhaustMap(url =>
        this._jsonLinkApi.getMetadata(url).pipe(
          prefix(() => this.patch({ status: ViewStatus.Loading })),
          tap({
            next: results =>
              this.patch({ status: ViewStatus.RetrievedMetadata, metadata: results }),
            error: err => this.patch({ status: ViewStatus.Error, error: err.message }),
          }),
          catchError(() => EMPTY)
        )
      )
    )
  );
}
