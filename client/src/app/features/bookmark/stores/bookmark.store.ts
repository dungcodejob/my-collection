import { Injectable, inject } from "@angular/core";

import { AuthStore } from "@core/authentication";
import { BookmarkApi } from "@core/http";
import { BookmarkDto, BookmarkQueryDto } from "@shared/models";
import { RouterStore } from "@shared/stores";
import { SignalStore, prefix } from "@shared/utils";
import { DialogConfig, PadDialogService } from "pad-ui-lib/dialog";
import {
  EMPTY,
  Observable,
  catchError,
  exhaustMap,
  filter,
  tap,
  withLatestFrom,
} from "rxjs";
import { CreateBookmarkDialogComponent } from "../components/bookmark-detail-dialog/bookmark-detail-dialog.component";

enum ViewStatus {
  Idle = "idle",
  Loading = "loading",
  Success = "success",
  Error = "error",
}

type BookmarkState = {
  status: ViewStatus;
  entities: BookmarkDto[];
  selectedId: BookmarkDto["id"] | null;
  error: string | null;
};

const initialState: BookmarkState = {
  status: ViewStatus.Idle,
  entities: [],
  selectedId: null,
  error: null,
};

@Injectable()
export class BookmarkSignalStore extends SignalStore<BookmarkState> {
  private readonly _bookmarkApi = inject(BookmarkApi);
  private readonly _authStore = inject(AuthStore);
  private readonly _dialogService = inject(PadDialogService);

  readonly $loading = this.select(
    state => state.status,
    status => status === "loading"
  );
  readonly $entities = this.select(state => state.entities);
  readonly $selectedId = this.select(state => state.selectedId);
  readonly $error = this.select(state => state.error);

  constructor() {
    super(initialState);
  }

  openAddDialog = this.effect((trigger$: Observable<void>) =>
    trigger$.pipe(
      exhaustMap(() => {
        this.patch({ selectedId: null });
        const config = new DialogConfig();
        config.injector = this._injector;
        return this._dialogService
          .open(CreateBookmarkDialogComponent, config)
          .afterClosed$.pipe(
            filter(Boolean),
            tap(value =>
              this.updater(state => ({ entities: [value, ...state.entities] }))
            )
          );
      })
    )
  );

  openUpdateDialog = this.effect((id$: Observable<string>) =>
    id$.pipe(
      exhaustMap(id => {
        this.patch({ selectedId: id });
        const config = new DialogConfig();
        config.injector = this._injector;
        return this._dialogService
          .open(CreateBookmarkDialogComponent, config)
          .afterClosed$.pipe(
            filter(Boolean),
            tap(value =>
              this._$state.entities.mutate(entities => {
                const indexToUpdate = entities.findIndex(
                  entity => entity.id === value.id
                );
                entities[indexToUpdate] = value;
              })
            )
          );
      })
    )
  );

  load = this.effect((trigger$: Observable<BookmarkQueryDto>) =>
    trigger$.pipe(
      withLatestFrom(RouterStore.getCollectionId()),
      exhaustMap(([query, collectionId]) => {
        const uid = this._authStore.$user().id;
        return this._bookmarkApi.fetchAll({ collectionId, uid }, query).pipe(
          prefix(() => this.patch({ status: ViewStatus.Loading })),
          tap({
            next: results =>
              this.patch({ status: ViewStatus.Success, entities: results.items }),
            error: err => this.patch({ status: ViewStatus.Error, error: err.message }),
          }),
          catchError(() => EMPTY)
        );
      })
    )
  );
}
