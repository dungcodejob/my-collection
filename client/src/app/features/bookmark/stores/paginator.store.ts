import { Injectable } from "@angular/core";

import { PaginationDto } from "@shared/models";
import { SignalStore, createEffect } from "@shared/utils";
import { Observable, tap } from "rxjs";

const DEFAULT_SIZE = 10;

type CursorType = PaginationDto["cursor"];
type PaginatorState = PaginationDto & {
  firstCursor: CursorType;
  lastCursor: CursorType;
  page: number;
};

const initial: PaginatorState = {
  firstCursor: null,
  lastCursor: null,
  cursor: null,
  direction: "next",
  size: DEFAULT_SIZE,
  page: 1,
};

@Injectable()
export class PaginatorStore extends SignalStore<PaginatorState> {
  readonly $pagination = this.selectMany(state => ({
    direction: state.direction,
    cursor: state.cursor,
    size: state.size,
  }));
  readonly $isPrev = this.select(
    state => state.page,
    page => page > 1
  );
  readonly $isNext = this.select(
    state => state.lastCursor,
    lastCursor => lastCursor != null
  );

  constructor() {
    super(initial);
  }

  setCursor = createEffect(
    (cursor$: Observable<{ first: CursorType; last: CursorType }>) =>
      cursor$.pipe(
        tap(({ first, last }) => this.patch({ firstCursor: first, lastCursor: last }))
      )
  );

  nextPage = createEffect(trigger$ =>
    trigger$.pipe(
      tap(() =>
        this.updater(state => ({
          ...state,
          cursor: state.lastCursor,
          direction: "next",
          page: state.page + 1,
        }))
      )
    )
  );

  previousPage = createEffect(trigger$ =>
    trigger$.pipe(
      tap(() =>
        this.updater(state => ({
          ...state,
          cursor: state.lastCursor,
          direction: "next",
          page: state.page + 1,
        }))
      )
    )
  );

  reset = createEffect(trigger$ => trigger$.pipe(tap(() => this.patch(initial))));
}
