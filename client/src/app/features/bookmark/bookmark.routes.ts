import { Routes } from "@angular/router";
import { BookmarkApi, JsonLinkApi } from "@core/http";
import { CreateBookmarkDialogComponent } from "./components/bookmark-detail-dialog/bookmark-detail-dialog.component";
import BookmarkManagementComponent from "./containers/bookmark-management/bookmark-management.component";

export const bookmarkRoutes: Routes = [
  {
    path: ":collectionId/bookmark",
    providers: [BookmarkApi, JsonLinkApi, CreateBookmarkDialogComponent],
    component: BookmarkManagementComponent,
  },
];
