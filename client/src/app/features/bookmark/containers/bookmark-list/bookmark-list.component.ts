import { NgFor, NgIf, NgTemplateOutlet } from "@angular/common";
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from "@angular/core";
import { RepeatDirective, TrackByModule } from "@shared/directives";
import { BookmarkDto } from "@shared/models";
import { PadButtonModule } from "pad-ui-lib/button";
import { PadFormModule } from "pad-ui-lib/form";
import { PadIconModule } from "pad-ui-lib/icon";
import { BookmarkSkeletonComponent } from "../../components/bookmark-skeleton/bookmark-skeleton.component";

@Component({
  selector: "app-bookmark-list",
  templateUrl: "./bookmark-list.component.html",
  styleUrls: ["./bookmark-list.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [
    NgIf,
    NgFor,
    PadButtonModule,
    PadFormModule,
    PadIconModule,
    TrackByModule,
    NgTemplateOutlet,
    BookmarkSkeletonComponent,
    RepeatDirective,
  ],
})
export class BookmarkListComponent {
  @Input() items: BookmarkDto[] = [];
  @Input() loading = true;
  @Input() error: string | null = null;
  @Input() isNext = false;
  @Input() isPrev = false;
  @Output() next = new EventEmitter<void>();
  @Output() edit = new EventEmitter<string>();
  @Output() prev = new EventEmitter<void>();
}
