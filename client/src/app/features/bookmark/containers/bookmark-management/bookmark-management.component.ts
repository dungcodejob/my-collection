import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
  inject,
} from "@angular/core";
import { PadDialogModule, PadDialogService } from "pad-ui-lib/dialog";

import { CdkMenuModule } from "@angular/cdk/menu";
import { NgFor } from "@angular/common";
import { BookmarkSignalStore, PaginatorStore } from "@features/bookmark/stores";
import { PadButtonModule } from "pad-ui-lib/button";
import { PadCheckboxModule } from "pad-ui-lib/checkbox";
import { PadFormModule } from "pad-ui-lib/form";
import { PadIconModule } from "pad-ui-lib/icon";
import { BookmarkListComponent } from "../bookmark-list/bookmark-list.component";

@Component({
  selector: "app-bookmark-management",
  templateUrl: "./bookmark-management.component.html",
  styleUrls: ["./bookmark-management.component.scss"],
  standalone: true,
  imports: [
    NgFor,
    CdkMenuModule,
    PadIconModule,
    PadButtonModule,
    PadFormModule,
    PadCheckboxModule,
    PadDialogModule,
    BookmarkListComponent,
  ],
  providers: [BookmarkSignalStore, PaginatorStore],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export default class BookmarkManagementComponent implements OnInit {
  private readonly _injector = inject(Injector);
  private readonly _dialogService = inject(PadDialogService);

  readonly bookmarkStore = inject(BookmarkSignalStore);
  readonly paginatorStore = inject(PaginatorStore);

  ngOnInit(): void {
    this.bookmarkStore.load({ direction: "next", cursor: null, size: 10 });
  }

  onOpenAddBookmarkDialog() {
    // const config = new DialogConfig();
    // config.injector = this._injector;
    // this.bookmarkStore.changeBookmarkId(null);
    // this._dialogService.open(CreateBookmarkDialogComponent, config);
    this.bookmarkStore.openAddDialog();
  }

  onOpenUpdateBookDialog(id: string) {
    this.bookmarkStore.openUpdateDialog(id);
  }
}
