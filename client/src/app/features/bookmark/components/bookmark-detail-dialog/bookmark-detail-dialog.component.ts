import { NgFor, NgIf } from "@angular/common";
import { Component, Injector, OnInit, effect, inject } from "@angular/core";

import { FormBuilder, ReactiveFormsModule, Validators } from "@angular/forms";

import { JsonLinkApi } from "@core/http/json-link.api";
import { BookmarkDetailSignalStore } from "@features/bookmark/stores";

import { LoadingComponent } from "@shared/components/loading.component";
import { BaseDto, BookmarkDto } from "@shared/models";
import { TypedFormGroup } from "@shared/utils";
import { PadButtonModule } from "pad-ui-lib/button";
import { DialogRef, PadDialogModule } from "pad-ui-lib/dialog";
import { PadFormModule } from "pad-ui-lib/form";
import { PadIconModule } from "pad-ui-lib/icon";

export type BookmarkForm = TypedFormGroup<Omit<BookmarkDto, keyof BaseDto>>;

@Component({
  templateUrl: "./bookmark-detail-dialog.component.html",
  styleUrls: ["./bookmark-detail-dialog.component.scss"],
  standalone: true,
  imports: [
    NgIf,
    NgFor,
    ReactiveFormsModule,
    PadButtonModule,
    PadIconModule,
    PadDialogModule,
    PadFormModule,
    LoadingComponent,
  ],
  providers: [JsonLinkApi, BookmarkDetailSignalStore],
})
export class CreateBookmarkDialogComponent implements OnInit {
  private readonly _bookmarkDetailStore = inject(BookmarkDetailSignalStore);
  private readonly _injector = inject(Injector);
  private readonly _ref = inject(DialogRef);
  private readonly _fb = inject(FormBuilder);

  $data = this._bookmarkDetailStore.$data;
  $metadata = this._bookmarkDetailStore.$metadata;
  $loading = this._bookmarkDetailStore.$loading;
  $success = this._bookmarkDetailStore.$success;
  $error = this._bookmarkDetailStore.$error;

  bookmarkForm!: BookmarkForm;

  ngOnInit(): void {
    // navigator.clipboard.readText().then(text => console.log(text));
    this._formInit();
    const data = this.$data();
    if (data) {
      this.bookmarkForm.patchValue(data);
    }

    effect(
      () => {
        if (this.$success()) {
          this._ref.close(this.$data());
        }
      },
      { injector: this._injector }
    );
    effect(
      () => {
        const metadata = this.$metadata();
        if (metadata) {
          this.bookmarkForm.patchValue({
            ...metadata,
            img: metadata.images && metadata.images.length > 0 ? metadata.images[0] : "",
          });
        }
      },
      { injector: this._injector }
    );
  }

  onClose() {
    this._ref.close();
  }

  onSave() {
    if (this.bookmarkForm.valid) {
      const data = this.bookmarkForm.getRawValue();

      this._bookmarkDetailStore.add(data);
    }
  }

  getMetadata() {
    const url = this.bookmarkForm.getRawValue().url;
    if (url) {
      this._bookmarkDetailStore.getMetadata(url);
    }
  }

  private _formInit(initial?: BookmarkDto) {
    this.bookmarkForm = this._fb.group({
      url: this._fb.control("", {
        nonNullable: true,
        validators: [Validators.required],
      }),
      title: this._fb.control("", {
        nonNullable: true,
        validators: [Validators.required],
      }),
      description: this._fb.control(""),
      domain: this._fb.control(
        { value: "", disabled: true },
        { nonNullable: true, validators: [Validators.required] }
      ),
      img: this._fb.control("", { nonNullable: true }),
      favicon: this._fb.control("", { nonNullable: true }),
    });

    if (initial) {
      this.bookmarkForm.patchValue(initial);
    }
  }
}
