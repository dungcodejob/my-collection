import { ChangeDetectionStrategy, Component } from "@angular/core";

@Component({
  selector: "app-bookmark-skeleton",
  templateUrl: "./bookmark-skeleton.component.html",
  styleUrls: ["./bookmark-skeleton.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
})
export class BookmarkSkeletonComponent {}
