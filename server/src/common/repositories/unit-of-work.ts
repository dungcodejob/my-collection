import { Inject, Injectable, Provider } from '@nestjs/common';

import { EntityManager } from '@mikro-orm/postgresql';
import {
  BookmarkRepository,
  BookmarkRepositoryImpl,
} from './bookmark.repository';
import {
  CollectionRepository,
  CollectionRepositoryImpl,
} from './collection.repository';
import { TagRepository, TagRepositoryImpl } from './tag.repository';
import { UserRepository, UserRepositoryImpl } from './user.repository';

export const UNIT_OF_WORK = Symbol('UnitOfWork');

export interface UnitOfWork {
  collection: CollectionRepository;
  bookmark: BookmarkRepository;
  tag: TagRepository;
  user: UserRepository;
  save(): Promise<void>;
}

@Injectable()
class UnitOfWorkImpl implements UnitOfWork {
  @Inject()
  private readonly _em: EntityManager;
  private readonly _collection: CollectionRepository;
  private readonly _bookmark: BookmarkRepository;
  private readonly _tag: TagRepository;
  private readonly _user: UserRepository;
  constructor() {}

  get collection(): CollectionRepository {
    if (this._collection) {
      return this._collection;
    }

    return new CollectionRepositoryImpl(this._em);
  }

  get bookmark(): BookmarkRepository {
    if (this._bookmark) {
      return this._bookmark;
    }

    return new BookmarkRepositoryImpl(this._em);
  }

  get tag(): TagRepository {
    if (this._tag) {
      return this._tag;
    }

    return new TagRepositoryImpl(this._em);
  }

  get user(): UserRepository {
    if (this._user) {
      return this._user;
    }

    return new UserRepositoryImpl(this._em);
  }

  save(): Promise<void> {
    return this._em.flush();
  }
}

export const UnitOfWorkProvider: Provider = {
  provide: UNIT_OF_WORK,
  useClass: UnitOfWorkImpl,
};
