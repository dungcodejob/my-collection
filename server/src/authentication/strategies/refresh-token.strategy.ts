import { UserEntity } from '@common/entities';
import { AuthConfig, InjectAuthConfig } from '@configs/index';
import { ForbiddenException, Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Request } from 'express';
import { ExtractJwt, Strategy } from 'passport-jwt';

import { AuthService, BcryptService } from '@authentication/services';
import { JwtPayload } from '../models';

@Injectable()
export class RefreshTokenStrategy extends PassportStrategy(
  Strategy,
  'jwt-refresh',
) {
  constructor(
    @InjectAuthConfig()
    authConfig: AuthConfig,
    private readonly _authService: AuthService,
    private readonly _bcryptService: BcryptService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: authConfig.jwtRefreshSecret,
      passReqToCallback: true,
    });
  }

  async validate(req: Request, payload: JwtPayload): Promise<UserEntity> {
    const refreshTokenHash = req
      .get('Authorization')
      .replace('Bearer', '')
      .trim();
    const user = await this._authService.validateUser(payload);
    if (!user || !user.refreshToken) {
      throw new ForbiddenException('Access Denied');
    }

    const isMatched = await this._bcryptService.verify(
      user.refreshToken,
      refreshTokenHash,
    );

    if (isMatched) {
      throw new ForbiddenException('Access Denied');
    }

    return user;
  }
}
