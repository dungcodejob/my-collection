import { UserModule } from '@modules/user';
import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { AuthService, BcryptService } from './services';
import { AccessTokenStrategy, RefreshTokenStrategy } from './strategies';

@Module({
  imports: [JwtModule, UserModule],
  providers: [
    AuthService,
    BcryptService,
    AccessTokenStrategy,
    RefreshTokenStrategy,
  ],
  exports: [AuthService, BcryptService],
})
export class AuthModule {}
