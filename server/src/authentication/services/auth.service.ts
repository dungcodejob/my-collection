import { JwtPayload, JwtToken } from '@authentication/models';
import { AuthConfig, InjectAuthConfig } from '@configs/index';
import { UserService } from '@modules/user';
import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { BcryptService } from './bcrypt.service';

@Injectable()
export class AuthService {
  constructor(
    @InjectAuthConfig()
    private readonly _authConfig: AuthConfig,
    private readonly _userService: UserService,
    private readonly _jwtService: JwtService,
    private readonly _bcryptService: BcryptService,
  ) {}

  async generateJwtToken(
    userId: string,
    payload: JwtPayload,
  ): Promise<JwtToken> {
    const createAccessToken = this._jwtService.signAsync(payload, {
      secret: this._authConfig.jwtSecret,
      expiresIn: this._authConfig.jwtExpired,
    });

    const createRefreshToken = this._jwtService.signAsync(payload, {
      secret: this._authConfig.jwtRefreshSecret,
      expiresIn: this._authConfig.jwtRefreshExpired,
    });

    const [accessToken, refreshToken] = await Promise.all([
      createAccessToken,
      createRefreshToken,
    ]);

    const refreshTokenHash = await this._bcryptService.hash(refreshToken);

    await this._userService.updateRefreshToken(userId, refreshTokenHash);

    return new JwtToken(accessToken, refreshTokenHash);
  }

  async validateUser(payload: JwtPayload) {
    const user = await this._userService.findByUsername(payload.username);

    if (user && user.email === payload.email) {
      return user;
    }
    return null;
  }
}
