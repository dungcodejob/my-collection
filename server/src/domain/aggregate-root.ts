import { Entity } from './base.entity';

export class AggregateRoot extends Entity {
  constructor(id: string) {
    super(id);
  }
}
