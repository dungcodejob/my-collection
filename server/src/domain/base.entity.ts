import { convertToPlainObject } from '@common/utils';

export abstract class Entity {
  private _id: string;
  private _createdAt: Date;
  private _updatedAt: Date;

  constructor(id: string) {
    const now = new Date();
    this._id = id;
    this._createdAt = now;
    this._updatedAt = now;
  }

  get id() {
    return this._id;
  }

  protected set id(id: string) {
    this._id = id;
  }

  get createdAt(): Date {
    return this._createdAt;
  }

  get updatedAt(): Date {
    return this._updatedAt;
  }

  static isEntity(entity: unknown): entity is Entity {
    return entity instanceof Entity;
  }

  equals(object?: Entity): boolean {
    if (object === null || object === undefined) {
      return false;
    }

    if (this === object) {
      return true;
    }

    if (!Entity.isEntity(object)) {
      return false;
    }

    return this.id ? this.id === object.id : false;
  }

  toObject() {
    const clone = structuredClone(this) as any;
    for (const prop in clone) {
      if (Array.isArray(clone[prop])) {
        clone[prop] = (clone[prop] as Array<unknown>).map((item) => {
          return convertToPlainObject(item);
        });
      }

      clone[prop] = convertToPlainObject(clone[prop]);
    }

    return clone;
  }
}
