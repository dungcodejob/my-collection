import { Migration } from '@mikro-orm/migrations';

export class Migration20231123041254 extends Migration {
  async up(): Promise<void> {
    this.addSql('alter table "tags" drop constraint "tags_user_id_foreign";');

    this.addSql(
      'alter table "tags" rename column "user_id" to "collection_id";',
    );
    this.addSql(
      'alter table "tags" add constraint "tags_collection_id_foreign" foreign key ("collection_id") references "collection" ("id") on update cascade;',
    );
  }

  async down(): Promise<void> {
    this.addSql(
      'alter table "tags" drop constraint "tags_collection_id_foreign";',
    );

    this.addSql(
      'alter table "tags" rename column "collection_id" to "user_id";',
    );
    this.addSql(
      'alter table "tags" add constraint "tags_user_id_foreign" foreign key ("user_id") references "users" ("id") on update cascade;',
    );
  }
}
