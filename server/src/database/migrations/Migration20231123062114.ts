import { Migration } from '@mikro-orm/migrations';

export class Migration20231123062114 extends Migration {

  async up(): Promise<void> {
    this.addSql('alter table "bookmarks" drop constraint "bookmarks_user_id_foreign";');

    this.addSql('alter table "bookmarks" rename column "user_id" to "collection_id";');
    this.addSql('alter table "bookmarks" add constraint "bookmarks_collection_id_foreign" foreign key ("collection_id") references "collection" ("id") on update cascade;');
  }

  async down(): Promise<void> {
    this.addSql('alter table "bookmarks" drop constraint "bookmarks_collection_id_foreign";');

    this.addSql('alter table "bookmarks" rename column "collection_id" to "user_id";');
    this.addSql('alter table "bookmarks" add constraint "bookmarks_user_id_foreign" foreign key ("user_id") references "users" ("id") on update cascade;');
  }

}
