import { MikroOrmModule } from '@mikro-orm/nestjs';
import { BookmarkModule } from '@modules/bookmark';
import { CollectionModule } from '@modules/collection';
import { SecurityModule } from '@modules/security';
import { TagModule } from '@modules/tag';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import {
  DatabaseConfig,
  appConfig,
  authConfig,
  databaseConfig,
} from './configs';
import { CrawlModule } from './modules/crawl';
import { UserModule } from './modules/user/user.module';
@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: `./src/configs/env/${process.env.NODE_ENV}.env`,
      load: [authConfig, appConfig, databaseConfig],
    }),
    MikroOrmModule.forRootAsync({
      inject: [databaseConfig.KEY],
      useFactory: (dbConfig: DatabaseConfig) => ({
        type: 'postgresql',
        host: dbConfig.host,
        port: dbConfig.port,
        username: dbConfig.username,
        password: dbConfig.password,
        dbName: dbConfig.dbName,

        debug: true,
        registerRequestContext: false,
        autoLoadEntities: true,
        allowGlobalContext: true,
        // entities: ['../../modules/**/entities/*.postgresql.entity.js'],
        // baseDir: __dirname,
      }),
    }),
    CrawlModule,
    UserModule,
    BookmarkModule,
    TagModule,
    SecurityModule,
    CollectionModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
