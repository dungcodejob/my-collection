import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';

import { MetadataDto } from '../../models/metadata.dto';
import { CrawlService } from '../../services';
import { GetMetadataQuery } from './get-metadata.query';

@QueryHandler(GetMetadataQuery)
export class GetMetadataHandler implements IQueryHandler<GetMetadataQuery> {
  constructor(private readonly crawlService: CrawlService) {}
  async execute(query: GetMetadataQuery): Promise<MetadataDto> {
    const { url } = query;
    return this.crawlService.getMetadata(url);
  }
}
