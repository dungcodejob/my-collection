import { GetMetadataHandler } from './get-metadata/get-metadata.handler';

export const QueryHandlers = [GetMetadataHandler];
