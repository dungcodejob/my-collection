import { Controller, Get, Query } from '@nestjs/common';
import { QueryBus } from '@nestjs/cqrs';
import { GetMetadataQuery } from './queries';

@Controller('crawl')
export class CrawlController {
  constructor(private readonly queryBus: QueryBus) {}

  @Get('metadata')
  async getMetadata(@Query('url') url: string) {
    const query = new GetMetadataQuery(url);
    return this.queryBus.execute(query);
  }
}
