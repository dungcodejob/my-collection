import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { CrawlController } from './crawl.controller';
import { QueryHandlers } from './queries/handlers';
import { CrawlService } from './services';

@Module({
  imports: [CqrsModule],
  providers: [CrawlService, ...QueryHandlers],
  controllers: [CrawlController],
})
export class CrawlModule {}
