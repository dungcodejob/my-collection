import { CurrentUser } from '@common/decorators';
import { UserEntity } from '@common/entities';
import { AccessTokenGuard } from '@common/guards';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { BookmarkFilterDto, BookmarkItemDto } from './models';
import { CreateBookmarkBodyDto } from './models/create-bookmark-body.dto';
import { BookmarkMapper, BookmarkService } from './services';

@UseGuards(AccessTokenGuard)
@Controller('bookmark')
export class BookmarkController {
  constructor(
    private readonly _bookmarkService: BookmarkService,
    private readonly _bookmarkMapper: BookmarkMapper,
  ) {}

  @Get()
  async getAll(
    @CurrentUser() user: UserEntity,
    @Query() dto: BookmarkFilterDto,
  ): Promise<BookmarkItemDto[]> {
    const bookmarkEntities = await this._bookmarkService.findAll(user.id, dto);

    return this._bookmarkMapper.toItemDto(bookmarkEntities);
  }

  @Put()
  async create(@Body() dto: CreateBookmarkBodyDto) {
    const bookmarkEntity = await this._bookmarkService.create(dto);

    return this._bookmarkMapper.toItemDto(bookmarkEntity);
  }

  @Delete(':id')
  async delete(@Param('id') id: string) {
    return this._bookmarkService.delete(id);
  }
}
