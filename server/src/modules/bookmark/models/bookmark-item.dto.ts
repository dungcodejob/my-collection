import { IdentityType } from '@database/identifiable.entity';

export interface BookmarkItemTagDto {
  readonly id: IdentityType;
  readonly title: string;
  readonly createAt: Date;
  readonly updateAt: Date;

  // static fromDomain(domain: TagEntity): BookmarkItemTagDto {
  //   return  {
  //     id: domain.id,
  //     title: domain.title,
  //     createAt:  domain.createAt,
  //     updateAt: domain.updateAt,
  //   }
  // }
}

export interface BookmarkItemDto {
  readonly id: IdentityType;
  readonly url: string;
  readonly domain: string;
  readonly title: string;
  readonly image: string;
  readonly description: string;
  readonly favicon: string;
  readonly note: string;
  readonly tags: BookmarkItemTagDto[];
  readonly createAt: Date;
  readonly updateAt: Date;

  // static fromDomain(domain: BookmarkEntity): BookmarkDto;
  // static fromDomain(domain: BookmarkEntity[]): BookmarkDto[];
  // static fromDomain(
  //   domain: BookmarkEntity | BookmarkEntity[],
  // ): BookmarkDto | BookmarkDto[] {
  //   if (Array.isArray(domain)) {
  //     return domain.map((item) => this.fromDomain(item));
  //   }

  //   const tags = domain.tags.map((tag) => BookmarkTagDto.fromDomain(tag));
  //   return new BookmarkDto(
  //     domain.id,
  //     domain.url,
  //     domain.domain,
  //     domain.title,
  //     domain.image,
  //     domain.description,
  //     domain.favicon,
  //     domain.note,
  //     tags,
  //     domain.createAt,
  //     domain.updateAt,
  //   );
  // }
}
