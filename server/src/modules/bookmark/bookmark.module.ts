import { BookmarkEntity } from '@common/entities';
import { UnitOfWorkProvider } from '@common/repositories';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { BookmarkController } from './bookmark.controller';
import { CommandHandlers } from './commands';
import { QueriesHandlers } from './queries';
import { BookmarkMapper, BookmarkService } from './services';

@Module({
  imports: [CqrsModule, MikroOrmModule.forFeature([BookmarkEntity])],
  providers: [
    ...CommandHandlers,
    ...QueriesHandlers,
    UnitOfWorkProvider,
    BookmarkService,
    BookmarkMapper,
  ],
  exports: [BookmarkMapper, BookmarkService],
  controllers: [BookmarkController],
})
export class BookmarkModule {}
