import { BookmarkEntity } from '@common/entities';
import { Injectable } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { CreateBookmarkCommand, DeleteBookmarkCommand } from '../commands';
import { BookmarkFilterDto, CreateBookmarkBodyDto } from '../models';
import { GetAllBookmarkQuery, GetBookmarkInCollectionQuery } from '../queries';

@Injectable()
export class BookmarkService {
  constructor(
    private readonly _commandBus: CommandBus,
    private readonly _queryBus: QueryBus,
  ) {}

  async findAll(
    userId: string,
    dto?: BookmarkFilterDto,
  ): Promise<BookmarkEntity[]> {
    const tagIds = dto && dto.tagIds ? dto.tagIds.split(',') : [];
    return this._queryBus.execute(new GetAllBookmarkQuery(userId, tagIds));
  }

  async findByCollectionId(
    collectionId: string,
    dto?: BookmarkFilterDto,
  ): Promise<BookmarkEntity[]> {
    const tagIds = dto && dto.tagIds ? dto.tagIds.split(',') : [];
    return this._queryBus.execute(
      new GetBookmarkInCollectionQuery(collectionId, tagIds),
    );
  }

  async create(dto: CreateBookmarkBodyDto): Promise<BookmarkEntity> {
    const command = new CreateBookmarkCommand(
      dto.url,
      dto.domain,
      dto.title,
      dto.image,
      dto.description,
      dto.favicon,
      dto.note,
      dto.collectionId,
      dto.tagIds,
    );

    return this._commandBus.execute(command);
  }
  async delete(id: string): Promise<void> {
    return this._commandBus.execute(new DeleteBookmarkCommand(id));
  }
}
