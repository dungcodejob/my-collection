import { CreateBookmarkHandler } from './create-bookmark/create-bookmark.handler';
import { DeleteBookmarkHandler } from './delete-bookmark/delete-bookmark.handler';

export const CommandHandlers = [CreateBookmarkHandler, DeleteBookmarkHandler];
