import { BadRequestException, UnauthorizedException } from '@nestjs/common';

export class SecurityErrors {
  static UserExists = new BadRequestException('User already exists');
  static UsernameOrPasswordNotMatched = new UnauthorizedException(
    'Username or password not match',
  );
}
