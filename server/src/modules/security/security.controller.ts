import { JwtPayload, JwtToken } from '@authentication/models';
import { AuthService } from '@authentication/services';
import { CurrentUser } from '@common/decorators';
import { UserEntity } from '@common/entities';
import { RefreshTokenGuard } from '@common/guards';
import { UserProfileDto } from '@modules/user';
import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { LoginBodyDto, RegisterBodyDto } from './models';
import { AuthResultDto } from './models/auth-result.dto';
import { SecurityErrors } from './security.errors';
import { SecurityService } from './security.service';

@Controller('security')
export class SecurityController {
  constructor(
    private readonly _authService: AuthService,
    private readonly _securityService: SecurityService,
  ) {}

  @Post('register')
  async register(@Body() body: RegisterBodyDto) {
    const isExists = await this._securityService.checkUserExisted(
      body.username,
    );
    if (isExists) {
      throw SecurityErrors.UserExists;
    }

    const newUser = await this._securityService.register(body);
    const payload: JwtPayload = {
      sub: newUser.id,
      username: newUser.username,
      email: newUser.email,
    };
    const tokens = await this._authService.generateJwtToken(
      newUser.id,
      payload,
    );

    return this._plantToAuthResult(tokens, newUser);
  }

  @Post('login')
  async login(@Body() body: LoginBodyDto) {
    const user = await this._securityService.getUserIfMatch(
      body.username,
      body.password,
    );

    if (!user) throw SecurityErrors.UsernameOrPasswordNotMatched;

    const payload: JwtPayload = {
      sub: user.id,
      username: user.username,
      email: user.email,
    };
    const tokens = await this._authService.generateJwtToken(user.id, payload);

    return this._plantToAuthResult(tokens, user);
  }

  @UseGuards(RefreshTokenGuard)
  @Post('refresh')
  async refreshTokens(@CurrentUser() user: UserEntity) {
    const tokens = await this._authService.generateJwtToken(user.id, {
      sub: user.id,
      email: user.email,
      username: user.username,
    });
    return tokens;
  }

  private _plantToAuthResult(tokens: JwtToken, user: UserEntity) {
    const result = new AuthResultDto();
    result.tokens = tokens;
    result.user = UserProfileDto.fromDomain(user);
    return result;
  }
}
