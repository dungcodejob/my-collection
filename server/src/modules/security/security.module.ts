import { AuthModule } from '@authentication/auth.module';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import { UserModule } from '@modules/user';
import { Module } from '@nestjs/common';
import { UserEntity } from 'src/common/entities/user.entity';
import { SecurityController } from './security.controller';
import { SecurityService } from './security.service';

@Module({
  imports: [MikroOrmModule.forFeature([UserEntity]), AuthModule, UserModule],
  providers: [SecurityService],
  controllers: [SecurityController],
  exports: [],
})
export class SecurityModule {}
