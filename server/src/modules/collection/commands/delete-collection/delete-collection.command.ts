export class DeleteCollectionCommand {
  constructor(public readonly collectionId: string) {}
}
