import { CurrentUser } from '@common/decorators';
import { UserEntity } from '@common/entities';
import { AccessTokenGuard } from '@common/guards';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import {
  CollectionItemDto,
  CreateCollectionBodyDto,
  UpdateCollectionBodyDto,
} from './models';

import {
  BookmarkFilterDto,
  BookmarkItemDto,
  BookmarkMapper,
  BookmarkService,
} from '@modules/bookmark';
import { TagItemDto, TagMapper, TagService } from '@modules/tag';
import { CollectionMapper, CollectionService } from './services';

@UseGuards(AccessTokenGuard)
@Controller('collection')
export class CollectionController {
  constructor(
    private readonly _collectionService: CollectionService,
    private readonly _collectionMapper: CollectionMapper,
    private readonly _bookmarkService: BookmarkService,
    private readonly _bookmarkMapper: BookmarkMapper,
    private readonly _tagService: TagService,
    private readonly _tapMapper: TagMapper,
  ) {}

  @Get('all')
  async getAll(@CurrentUser() user: UserEntity): Promise<CollectionItemDto[]> {
    const collectionEntities = await this._collectionService.findAll(user.id);

    return this._collectionMapper.toItemDto(collectionEntities);
  }

  @Get(':id/tags')
  async getTags(
    @CurrentUser() user: UserEntity,
    @Param('id') id: string,
  ): Promise<TagItemDto[]> {
    const tagEntities = await this._tagService.findByCollectionId(id);

    return this._tapMapper.toItemDto(tagEntities);
  }

  @Get(':id/bookmarks')
  async getBookmarks(
    @Param('id') id: string,
    dto: BookmarkFilterDto,
  ): Promise<BookmarkItemDto[]> {
    const bookmarkEntities = await this._bookmarkService.findByCollectionId(
      id,
      dto,
    );

    return this._bookmarkMapper.toItemDto(bookmarkEntities);
  }

  @Put()
  async create(
    @CurrentUser() user: UserEntity,
    @Body() dto: CreateCollectionBodyDto,
  ): Promise<CollectionItemDto> {
    const collectionEntity = await this._collectionService.create(user.id, dto);
    return this._collectionMapper.toItemDto(collectionEntity);
  }

  @Post(':id')
  async update(@Param('id') id: string, @Body() dto: UpdateCollectionBodyDto) {
    const collectionEntity = await this._collectionService.update(id, dto);
    return this._collectionMapper.toItemDto(collectionEntity);
  }

  @Delete(':id')
  async delete(@Param('id') id: string): Promise<void> {
    return this._collectionService.delete(id);
  }
}
