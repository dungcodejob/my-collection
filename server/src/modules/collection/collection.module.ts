import { CollectionEntity } from '@common/entities';
import { UnitOfWorkProvider } from '@common/repositories';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import { BookmarkModule } from '@modules/bookmark';
import { TagModule } from '@modules/tag';
import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { CollectionController } from './collection.controller';
import { CommandHandlers } from './commands';
import { QueriesHandlers } from './queries';
import { CollectionMapper, CollectionService } from './services';

@Module({
  imports: [
    CqrsModule,
    MikroOrmModule.forFeature([CollectionEntity]),
    TagModule,
    BookmarkModule,
  ],
  providers: [
    ...CommandHandlers,
    ...QueriesHandlers,
    CollectionService,
    CollectionMapper,
    UnitOfWorkProvider,
  ],
  exports: [CollectionService, CollectionMapper],
  controllers: [CollectionController],
})
export class CollectionModule {}
