import { GetAllCollectionHandler } from './get-all-collection/get-all-collection.handler';

export const QueriesHandlers = [GetAllCollectionHandler];
