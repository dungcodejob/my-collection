export interface CollectionItemDto {
  readonly id: string;
  readonly title: string;
  readonly createAt: Date;
  readonly updateAt: Date;
}
