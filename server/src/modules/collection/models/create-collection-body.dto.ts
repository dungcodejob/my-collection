export interface CreateCollectionBodyDto {
  readonly title: string;
  readonly parentId?: string;
}
