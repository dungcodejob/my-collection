export interface CollectionDto {
  readonly id: string;
  readonly title: string;
  readonly createAt: Date;
  readonly updateAt: Date;
}
