import { UnitOfWorkProvider } from '@common/repositories';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { UserEntity } from 'src/common/entities/user.entity';
import { CommandHandlers } from './commands';
import { QueriesHandlers } from './queries';
import { UserService } from './user.service';

@Module({
  imports: [CqrsModule, MikroOrmModule.forFeature([UserEntity])],
  providers: [
    ...CommandHandlers,
    ...QueriesHandlers,
    UnitOfWorkProvider,
    UserService,
  ],
  exports: [UserService],
  controllers: [],
})
export class UserModule {}
