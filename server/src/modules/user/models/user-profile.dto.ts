import { UserEntity } from '@common/entities';

export class UserProfileDto {
  constructor(
    public readonly id: string,
    public readonly firstName: string,
    public readonly lastName: string,
  ) {}

  static fromDomain(domain: UserEntity): UserProfileDto {
    return new UserProfileDto(domain.id, domain.firstName, domain.lastName);
  }
}
