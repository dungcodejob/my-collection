import { BadRequestException } from '@nestjs/common';

export class UserErrors {
  static UserNotExist = new BadRequestException('User does not exist');
}
