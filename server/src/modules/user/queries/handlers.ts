import { FindUserByUsernameHandler } from './find-user-by-username/find-user-by-username.handler';

export const QueriesHandlers = [FindUserByUsernameHandler];
