import { TagEntity } from '@common/entities';
import { Injectable } from '@nestjs/common';
import { TagDto, TagItemDto } from '../models';

@Injectable()
export class TagMapper {
  toItemDto(domain: TagEntity): TagItemDto;
  toItemDto(domain: TagEntity[]): TagItemDto[];
  toItemDto(domain: TagEntity | TagEntity[]): TagItemDto | TagItemDto[] {
    if (Array.isArray(domain)) {
      return domain.map((item) => this.toItemDto(item));
    }
    return {
      id: domain.id,
      title: domain.title,
      createAt: domain.createAt,
      updateAt: domain.updateAt,
    };
  }

  toDto(domain: TagEntity): TagDto {
    return {
      id: domain.id,
      title: domain.title,
      createAt: domain.createAt,
      updateAt: domain.updateAt,
    };
  }
}
