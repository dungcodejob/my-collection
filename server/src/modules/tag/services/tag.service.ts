import { TagEntity } from '@common/entities';
import { Injectable } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { CreateTagCommand } from '../commands';
import { CreateTagDto } from '../models';
import { GetAllTagQuery, GetTagInCollectionQuery } from '../queries';

@Injectable()
export class TagService {
  constructor(
    private readonly _commandBus: CommandBus,
    private readonly _queryBus: QueryBus,
  ) {}

  async findAll(userId: string): Promise<TagEntity[]> {
    return this._queryBus.execute(new GetAllTagQuery(userId));
  }

  async findByCollectionId(collectionId: string): Promise<TagEntity[]> {
    return this._queryBus.execute(new GetTagInCollectionQuery(collectionId));
  }

  async create(dto: CreateTagDto): Promise<TagEntity> {
    return this._commandBus.execute(
      new CreateTagCommand(dto.title, dto.collectionId),
    );
  }
}
