import { CurrentUser } from '@common/decorators';
import { UserEntity } from '@common/entities';
import { AccessTokenGuard } from '@common/guards';
import { Body, Controller, Get, Put, UseGuards } from '@nestjs/common';
import { CreateTagDto } from './models';
import { TagMapper, TagService } from './services';

@UseGuards(AccessTokenGuard)
@Controller('tag')
export class TagController {
  constructor(
    private readonly _tagService: TagService,
    private readonly _tagMapper: TagMapper,
  ) {}

  @Put()
  async create(@Body() body: CreateTagDto) {
    const tagEntity = await this._tagService.create(body);
    return this._tagMapper.toItemDto(tagEntity);
  }

  @Get('all')
  async GetAll(@CurrentUser() user: UserEntity) {
    const tagEntities = await this._tagService.findAll(user.id);
    return this._tagMapper.toItemDto(tagEntities);
  }
}
