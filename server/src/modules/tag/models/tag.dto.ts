export interface TagDto {
  readonly id: string;
  readonly title: string;
  readonly updateAt: Date;
  readonly createAt: Date;
}
