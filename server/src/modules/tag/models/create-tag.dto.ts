export interface CreateTagDto {
  readonly title: string;
  readonly collectionId: string;
}
