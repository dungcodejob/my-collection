export interface TagItemDto {
  readonly id: string;
  readonly title: string;
  readonly updateAt: Date;
  readonly createAt: Date;
}
