import { Inject } from '@nestjs/common';
import { EventPublisher, IQueryHandler, QueryHandler } from '@nestjs/cqrs';

import { TagEntity } from '@common/entities';

import { UNIT_OF_WORK, UnitOfWork } from '@common/repositories';
import { GetTagInCollectionQuery } from './get-tag-in-collection.query';

@QueryHandler(GetTagInCollectionQuery)
export class GetTagInCollectionHandler
  implements IQueryHandler<GetTagInCollectionQuery>
{
  constructor(
    private readonly eventPublisher: EventPublisher,
    @Inject(UNIT_OF_WORK) private readonly _unitOfWork: UnitOfWork,
  ) {}
  async execute(query: GetTagInCollectionQuery): Promise<TagEntity[]> {
    return this._unitOfWork.tag.findByCollectionId(query.collectionId);
  }
}
