import { GetAllTagHandler } from './get-all-tag/get-all-tag.handler';
import { GetTagInCollectionHandler } from './get-tag-in-collection/get-tag-in-collection.handler';

export const QueriesHandlers = [GetAllTagHandler, GetTagInCollectionHandler];
