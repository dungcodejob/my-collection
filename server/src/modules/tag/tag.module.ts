import { TagEntity } from '@common/entities';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';

import { UnitOfWorkProvider } from '@common/repositories';
import { CommandHandlers } from './commands/handlers';
import { QueriesHandlers } from './queries';
import { TagMapper, TagService } from './services';
import { TagController } from './tag.controller';

@Module({
  imports: [CqrsModule, MikroOrmModule.forFeature([TagEntity])],
  providers: [
    ...CommandHandlers,
    ...QueriesHandlers,
    TagMapper,
    TagService,
    UnitOfWorkProvider,
  ],
  exports: [TagMapper, TagService],
  controllers: [TagController],
})
export class TagModule {}
